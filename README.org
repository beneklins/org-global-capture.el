* org-global-capture.el

An Emacs package that lets you use org-capture from everywhere in your system.

** History
The original code was written by Phillip J. Windley and was licensed under the
CC BY-NC-SA 1.0 licence. It was later licensed under the CC BY-SA 4.0 licence:
- CC 1.0 (PJW LC.): https://web.archive.org/web/20110108074101/https://www.windley.com/archives/2010/12/capture_mode_and_emacs.shtml
- CC 4.0 (Phillip J. Windley): https://web.archive.org/web/20220526191327/https://www.windley.com/archives/2010/12/capture_mode_and_emacs.shtml

Later modifications were made by Sergey Pashinin (a derivative work with no
licensing information):
- https://gitlab.com/pashinin/emacsd/-/blob/master/elisp/init-org-capture.el

The original Emacs package ~org-global-capture~ was written by Lukas
Woell/Isimoro | Lou Woell/einsiedlerspiel using the previous codes and
licensed under the GPL v3.
- https://web.archive.org/web/20200920141535/https://github.com/Isimoro/org-global-capture.el
- https://web.archive.org/web/20201112015130/https://github.com/Isimoro/org-global-capture.el/blob/master/org-global-capture.el
- https://web.archive.org/web/20220714122823/https://github.com/einsiedlerspiel/org-global-capture.el
